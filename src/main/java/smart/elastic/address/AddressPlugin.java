package smart.elastic.address;

import org.elasticsearch.common.inject.Module;
import org.elasticsearch.plugins.AbstractPlugin;
import org.elasticsearch.rest.RestModule;

import smart.elastic.address.metro.MetroAction;

public class AddressPlugin extends AbstractPlugin {

    @Override
    public String description() {
        return "address search plugin";
    }

    @Override
    public String name() {
        return "address";
    }

    @Override
    public void processModule(Module module) {
        if (module instanceof RestModule) {
            ((RestModule) module).addRestAction(AddressAction.class);
            ((RestModule) module).addRestAction(MetroAction.class);
        }
    }
}
