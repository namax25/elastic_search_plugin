package smart.elastic.address.config;

import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

@SuppressWarnings("unchecked")
public class Config {
    private final static Map<String, Object> config = new HashMap<String, Object>();

    static {
        Yaml yaml = new Yaml();
        try {
            config.putAll((Map<String, Object>) yaml.load(ClassLoader.getSystemResource("config.yaml").openConnection().getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object get(String key) {
        return config.get(key);
    }
}
