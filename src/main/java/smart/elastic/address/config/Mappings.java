package smart.elastic.address.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import smart.elastic.address.Type;

@SuppressWarnings("unchecked")
public class Mappings {

    private final static List<String> countyTypes = new ArrayList<String>();

    private final static List<String> fullAddressCountyTypes = new ArrayList<String>();

    private final static List<String> streetTypes = new ArrayList<String>();

    private final static List<String> metroTypes = new ArrayList<String>();

    private final static Map<String, String> mappings = new HashMap<String, String>();

    private final static Map<String, String> metroMappings = new HashMap<String, String>();

    private final static Map<String, Boolean> countyTypesMap = new HashMap<String, Boolean>();

    private final static Map<String, Boolean> streetTypesMap = new HashMap<String, Boolean>();

    private final static Map<String, String> outputMappings = new HashMap<String, String>();

    static {
        Map<String, Object> mappings = (Map<String, Object>) Config.get("mappings");
        if (mappings != null) {
            List<String> counties = (List<String>) mappings.get("counties");
            if (counties != null)
                countyTypes.addAll(counties);

            for (String type : countyTypes)
                countyTypesMap.put(type, true);

            List<String> fullAddress = (List<String>) mappings.get("full_address_counties");
            if (fullAddress != null)
                fullAddressCountyTypes.addAll(fullAddress);

            List<String> streets = (List<String>) mappings.get("streets");
            if (counties != null)
                streetTypes.addAll(streets);

            List<String> metro = (List<String>) mappings.get("metro");
            if (metro != null)
                metroTypes.addAll(metro);

            for (String type : streetTypes)
                streetTypesMap.put(type, true);

            Map<String, String> sm = (Map<String, String>) mappings.get("short_mappings");
            if (sm != null)
                Mappings.mappings.putAll(sm);

            Map<String, String> om = (Map<String, String>) mappings.get("output_replacements");
            if (om != null)
                Mappings.outputMappings.putAll(om);

            Map<String, String> mm = (Map<String, String>) mappings.get("metro_mappings");
            if (mm != null)
                Mappings.metroMappings.putAll(mm);
        }
    }

    public static List<String> getCountyTypes() {
        return countyTypes;
    }

    public static List<String> getStreetTypes() {
        return streetTypes;
    }

    public static List<String> getMetroTypes() {
        return metroTypes;
    }

    public static List<String> getObjectTypes(Type type) {
        switch (type) {
            case COUNTY:
                return getCountyTypes();
            case STREET:
                return getStreetTypes();
            case METRO:
                return getMetroTypes();
        }
        return null;
    }

    public static Map<String, Boolean> getCountyTypesMap() {
        return countyTypesMap;
    }

    public static Map<String, Boolean> getStreetTypesMap() {
        return streetTypesMap;
    }

    public static List<String> getFulladdressCountyTypes() {
        return fullAddressCountyTypes;
    }

    public static String getMapping(String key) {
        if (mappings.containsKey(key))
            return mappings.get(key);
        return key;
    }

    public static String getMetroMapping(String key) {
        if (metroMappings.containsKey(key))
            return metroMappings.get(key);
        return key;
    }

    public static Map<String, String> getOutputMappings() {
        return outputMappings;
    }
}
