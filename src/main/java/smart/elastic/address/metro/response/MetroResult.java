package smart.elastic.address.metro.response;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import smart.elastic.address.response.Item;
import smart.elastic.address.text.TextResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MetroResult {
    private static final Type RESULT_TYPE = new TypeToken<MetroResult>() {
    }.getType();

    public static final MetroResult NULL = new MetroResult(null, null);

    private static final Gson GSON = new Gson();

    private final List<Item> stations;

    private final List<Integer> ids;

    private final String originalText;

    private final String text;

    public List<Item> getStations() {
        return stations;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public String getOriginalText() {
        return originalText;
    }

    public String getText() {
        return text;
    }

    public MetroResult(TextResponse text, List<Item> stations) {
        this.text = text == null ? null : text.getCleanedText();
        this.originalText = text == null ? null : text.getOriginalText();
        this.stations = stations;
        if (stations != null) {
            List<Integer> ids = new ArrayList<Integer>();
            for (Item s : stations)
                ids.add(s.getId());
            this.ids = ids;
        } else {
            this.ids = null;
        }
    }

    public String toJSON() {
        return GSON.toJson(this, RESULT_TYPE);
    }
}
