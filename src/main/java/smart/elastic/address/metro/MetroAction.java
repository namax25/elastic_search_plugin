package smart.elastic.address.metro;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.RestRequest.Method;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.rest.StringRestResponse;
import org.elasticsearch.rest.XContentThrowableRestResponse;

import com.google.common.base.Splitter;

import smart.elastic.address.Type;
import smart.elastic.address.config.Config;
import smart.elastic.address.metro.response.MetroResult;
import smart.elastic.address.response.Item;
import smart.elastic.address.search.SearchExecutor;
import smart.elastic.address.search.SearchType;
import smart.elastic.address.text.NGram;
import smart.elastic.address.text.TextProcessor;
import smart.elastic.address.text.TextResponse;

public class MetroAction extends BaseRestHandler {

    private int metroMaxTokenSize = Integer.valueOf(Config.get("metroMaxTokenSize").toString());

    private int minTokenLength = Integer.valueOf(Config.get("minTokenLength").toString());

    private SearchExecutor searchExecutor = new SearchExecutor(client, Config.get("index").toString());

    @Inject
    protected MetroAction(Settings settings, Client client, RestController controller) {
        super(settings, client);
        controller.registerHandler(Method.GET, "/_metro", this);
    }

    @Override
    public void handleRequest(RestRequest request, RestChannel channel) {
        try {
            TextResponse text = TextProcessor.getInstance().process(searchExecutor, request.param("text"), Type.METRO);
            if (text == null) {
                channel.sendResponse(new StringRestResponse(RestStatus.OK, MetroResult.NULL.toJSON()));
                return;
            }
            List<Integer> regions = null;
            if (request.param("regions") != null) {
                regions = new ArrayList<Integer>();
                for (String p : Splitter.on(',').omitEmptyStrings().trimResults().split(request.param("regions")))
                    regions.add(Integer.valueOf(p));
            }
            if (regions == null || regions.isEmpty())
                regions = Arrays.asList(46, 47);

            List<Item> stations = new ArrayList<Item>();
            for (int i = metroMaxTokenSize; i > 0; i--) {
                List<NGram> nGrams = TextProcessor.getInstance().filterNGrams(TextProcessor.getInstance().getNGrams(i, text, Type.METRO), minTokenLength);
                for (SearchType searchType : Arrays.asList(SearchType.ORIGINAL, SearchType.INDEXED)) {
                    for (NGram nGram : nGrams) {
                        if (nGram.getTokens().size() < 2 && SearchType.INDEXED.equals(searchType))
                            continue;
                        List<Item> items = searchExecutor.searchMetro(nGram, searchType, text, regions);
                        if (items.size() == 0)
                            continue;
                        stations.addAll(analyzeResponse(items));
                    }
                }
            }
            channel.sendResponse(new StringRestResponse(RestStatus.OK, new MetroResult(text, stations).toJSON()));
        } catch (Exception e) {
            try {
                e.printStackTrace();
                channel.sendResponse(new XContentThrowableRestResponse(request, e));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    private List<Item> analyzeResponse(List<Item> stations) {
        List<Item> matched = new ArrayList<Item>();
        for (Item station : stations) {
            if (station.getOriginalTokens().size() != station.getnGram().getTokens().size())
                continue;
            matched.add(station);
        }
        return matched;
    }

}
