package smart.elastic.address.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.search.SearchHit;

import smart.elastic.address.search.SearchType;
import smart.elastic.address.text.NGram;
import smart.elastic.address.text.TextProcessor;
import smart.elastic.address.text.TextResponse;

import com.google.common.base.Joiner;

public class Item {

    private final int id;

    private final String originalName;

    private final String shortName;

    private final transient List<String> analyzedTokens;

    private final transient List<String> originalTokens;

    private final transient List<String> fullOriginalTokens;

    private final transient List<String> fullAddressTokens;

    private final List<Integer> parents;

    private final transient List<Integer> realParents;

    private final NGram nGram;

    private final transient String token;

    private final SearchType searchType;

    private final transient TextResponse text;

    public Item(int id, SearchType searchType, String originalName, String shortName, List<String> analyzedTokens, List<String> originalTokens, List<String> fullOriginalTokens, List<String> fullAddressTokens, List<Integer> parents, NGram nGram, TextResponse text) {
        this.id = id;
        this.searchType = searchType;
        this.token = Joiner.on(TextProcessor.NBSP).join(originalTokens);
        this.originalName = originalName;
        this.shortName = shortName;
        this.analyzedTokens = analyzedTokens;
        this.fullAddressTokens = fullAddressTokens;
        this.originalTokens = originalTokens;
        this.fullOriginalTokens = fullOriginalTokens;
        this.parents = parents;
        if (parents != null) {
            if (parents.size() < 2)
                this.realParents = Collections.emptyList();
            else
                this.realParents = parents.subList(0, parents.size() - 2);
        } else {
            this.realParents = null;
        }
        this.nGram = nGram;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public String getShortName() {
        return shortName;
    }

    public List<String> getAnalyzedTokens() {
        return analyzedTokens;
    }

    public List<String> getOriginalTokens() {
        return originalTokens;
    }

    public List<Integer> getParents() {
        return parents;
    }

    public NGram getnGram() {
        return nGram;
    }

    public String getToken() {
        return token;
    }

    public List<Integer> getRealParents() {
        return realParents;
    }

    public static Item fromSearchHit(NGram nGram, SearchHit hit, SearchType searchType, TextResponse text) {
        return fromSource(hit.getId(), searchType, hit.getSource(), nGram, text);
    }


    public static Item fromGetResponse(GetResponse getResponse, TextResponse text) {
        return fromSource(getResponse.getId(), null, getResponse.getSource(), null, text);
    }

    @SuppressWarnings("unchecked")
    private static Item fromSource(String id, SearchType searchType, Map<String, Object> source, NGram nGram, TextResponse text) {
        List<Integer> parents = null;
        if (source.get("parents") != null) {
            parents = new ArrayList<Integer>();
            for (Map<String, Object> p : (List<Map<String, Object>>) source.get("parents"))
                parents.add(Integer.valueOf(p.get("id").toString()));
        }
        return new Item(Integer.valueOf(id), searchType, source.get("original_variant").toString(), source.get("short_name").toString(), (List<String>) source.get("name"), (List<String>) source.get("original_name"), (List<String>) source.get("original_full_name"), (List<String>) source.get("full_address"), parents, nGram, text);
    }

    public List<String> getFullAddressTokens() {
        return fullAddressTokens;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public List<String> getFullOriginalTokens() {
        return fullOriginalTokens;
    }

    public TextResponse getText() {
        return text;
    }
}
