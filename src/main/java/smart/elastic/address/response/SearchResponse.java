package smart.elastic.address.response;

import java.util.List;


public class SearchResponse {

    public final static SearchResponse NULL = new SearchResponse(null, null, null);

    private Item county;

    private Item street;

    private List<Item> streets;

    public SearchResponse(Item county, Item street, List<Item> streets) {
        this.county = county;
        this.street = street;
        this.streets = streets;
    }

    public Item getCounty() {
        return county;
    }

    public void setCounty(Item county) {
        this.county = county;
    }

    public Item getStreet() {
        return street;
    }

    public void setStreet(Item street) {
        this.street = street;
    }

    public List<Item> getStreets() {
        return streets;
    }

    public void setStreets(List<Item> streets) {
        this.streets = streets;
    }
}
