package smart.elastic.address.response;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import smart.elastic.address.text.TextResponse;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Result {
    private static final Type RESULT_TYPE = new TypeToken<Result>() {
    }.getType();

    public static final Result NULL = new Result(null, null, null, null, null, null, null);

    private static final Gson GSON = new Gson();

    private final Item county;

    private final Item street;

    private final List<Item> streets;

    private final String cacheName;

    private final List<Integer> streetIds;

    private final String originalText;

    private final String text;

    private final String house;

    private final String corpus;

    public Item getCounty() {
        return county;
    }

    public Item getStreet() {
        return street;
    }

    public List<Item> getStreets() {
        return streets;
    }

    public String getCacheName() {
        return cacheName;
    }

    public List<Integer> getStreetIds() {
        return streetIds;
    }

    public String getOriginalText() {
        return originalText;
    }

    public String getText() {
        return text;
    }

    public String getHouse() {
        return house;
    }

    public String getCorpus() {
        return corpus;
    }

    public Result(TextResponse text, Item county, Item street, List<Item> streets, String cacheName, String house, String corpus) {
        this.text = text == null ? null : text.getCleanedText();
        this.originalText = text == null ? null : text.getOriginalText();
        this.county = county;
        this.street = street;
        this.streets = streets;
        this.cacheName = cacheName;
        if (streets != null) {
            List<Integer> ids = new ArrayList<Integer>();
            for (Item s : streets)
                ids.add(s.getId());
            this.streetIds = ids;
        } else {
            streetIds = null;
        }
        this.house = house;
        this.corpus = corpus;
    }

    public String toJSON() {
        return GSON.toJson(this, RESULT_TYPE);
    }
}
