package smart.elastic.address;

public enum Type {
    COUNTY, STREET, METRO
}
