package smart.elastic.address.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeRequest;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeResponse;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeResponse.AnalyzeToken;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import smart.elastic.address.Type;
import smart.elastic.address.config.Mappings;
import smart.elastic.address.response.Item;
import smart.elastic.address.text.NGram;
import smart.elastic.address.text.TextResponse;

public class SearchExecutor {

    private final String index;

    private final Client client;

    public SearchExecutor(Client client, String index) {
        this.index = index;
        this.client = client;
    }

    public List<Item> search(NGram nGram, Type type, List<Integer> parents, SearchType searchType, TextResponse text) throws Exception {
        if (nGram == null)
            return Collections.emptyList();
        final SearchRequest searchRequest = Requests.searchRequest(index).searchType(org.elasticsearch.action.search.SearchType.QUERY_THEN_FETCH).types(type.name().toLowerCase());
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().size(30);
        QueryBuilder queryBuilder = buildSearchQuery(nGram, type, parents, searchType);
        if (queryBuilder == null)
            return Collections.emptyList();
        sourceBuilder.query(queryBuilder);
        searchRequest.source(sourceBuilder);
        SearchResponse result = client.search(searchRequest).get();

        List<Item> response = new ArrayList<Item>();
        for (SearchHit hit : result.getHits())
            response.add(Item.fromSearchHit(nGram, hit, searchType, text));
        return response;
    }

    public List<Item> searchMetro(NGram nGram, SearchType searchType, TextResponse text, List<Integer> regions) throws Exception {
        if (nGram == null)
            return Collections.emptyList();
        final SearchRequest searchRequest = Requests.searchRequest(index).searchType(org.elasticsearch.action.search.SearchType.QUERY_THEN_FETCH).types(Type.METRO.name().toLowerCase());
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().size(30);
        QueryBuilder queryBuilder = buildMetroQuery(nGram, regions, searchType);
        if (queryBuilder == null)
            return Collections.emptyList();
        sourceBuilder.query(queryBuilder);
        searchRequest.source(sourceBuilder);
        SearchResponse result = client.search(searchRequest).get();

        List<Item> response = new ArrayList<Item>();
        for (SearchHit hit : result.getHits())
            response.add(Item.fromSearchHit(nGram, hit, searchType, text));
        return response;
    }

    public List<Item> searchInFullAddress(Type type, NGram nGram, List<Integer> parents, TextResponse text) throws InterruptedException, ExecutionException {
        final SearchRequest searchRequest = Requests.searchRequest(index).searchType(org.elasticsearch.action.search.SearchType.QUERY_THEN_FETCH).types(type.name().toLowerCase());
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder().size(30);
        QueryBuilder queryBuilder = buildFullAddressQuery(type, nGram, parents);
        sourceBuilder.query(queryBuilder);
        searchRequest.source(sourceBuilder);
        SearchResponse result = client.search(searchRequest).get();

        List<Item> response = new ArrayList<Item>();
        for (SearchHit hit : result.getHits())
            response.add(Item.fromSearchHit(nGram, hit, null, text));
        return response;
    }

    public List<String> analyzeText(String text) {
        AnalyzeRequest ar = new AnalyzeRequest(index, text);
        ar.analyzer("main");
        ActionFuture<AnalyzeResponse> result = client.admin().indices().analyze(ar);
        try {
            List<String> response = new ArrayList<String>();
            for (AnalyzeToken token : result.get().getTokens())
                response.add(token.getTerm());
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public List<Item> getParents(Type type, List<Integer> ids) throws InterruptedException, ExecutionException {
        List<Item> result = new ArrayList<Item>();
        MultiGetRequest request = new MultiGetRequest();
        for (Integer id : ids)
            request.add(index, type.name().toLowerCase(), id.toString());
        MultiGetResponse response = client.multiGet(request).get();
        for (MultiGetItemResponse item : response.getResponses()) {
            if (!item.getResponse().isExists())
                continue;
            result.add(Item.fromGetResponse(item.getResponse(), null));
        }
        return result;
    }

    private QueryBuilder buildMetroQuery(NGram nGram, List<Integer> regions, SearchType searchType) {
        List<String> tokens = searchType.getTokens(nGram);
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        for (String token : tokens)
            boolQuery.must(QueryBuilders.termQuery(searchType.getField(), token));
        if (regions.size() > 0)
            boolQuery.must(QueryBuilders.termsQuery("regions.id", regions.toArray()));
        return boolQuery;
    }

    private QueryBuilder buildSearchQuery(NGram nGram, Type type, List<Integer> parents, SearchType searchType) {
        List<String> tokens = searchType.getTokens(nGram);
        //если ищем по одному слову, то нет смысла искать по полному названию
        if (tokens.size() == 1 && (SearchType.FULL_INDEXED.equals(searchType) || SearchType.FULL_ORIGINAL.equals(searchType)))
            return null;
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        for (String token : tokens)
            boolQuery.must(QueryBuilders.termQuery(searchType.getField(), token));
        if (parents.size() > 0)
            boolQuery.must(QueryBuilders.termsQuery("parents.id", parents.toArray()));

        //типы объектов
        List<String> types = Type.COUNTY.equals(type) ? Mappings.getCountyTypes() : Mappings.getStreetTypes();
        if (Type.COUNTY.equals(type) && (SearchType.FULL_INDEXED.equals(searchType) || SearchType.FULL_ORIGINAL.equals(searchType)))
            types = Mappings.getFulladdressCountyTypes();
        for (String typeName : types)
            boolQuery.should(QueryBuilders.termQuery("short_name", typeName));
        boolQuery.minimumNumberShouldMatch(1);
        return boolQuery;
    }

    private QueryBuilder buildFullAddressQuery(Type type, NGram nGram, List<Integer> parents) {
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        for (String token : nGram.getAnalyzedTokens())
            boolQuery.must(QueryBuilders.termQuery("full_address", token));
        if (parents.size() > 0)
            boolQuery.must(QueryBuilders.termsQuery("parents.id", parents.toArray()));

        for (String typeName : Type.STREET.equals(type) ? Mappings.getStreetTypes() : Mappings.getFulladdressCountyTypes())
            boolQuery.should(QueryBuilders.termQuery("short_name", typeName));
        boolQuery.minimumNumberShouldMatch(1);
        return boolQuery;
    }

}
