package smart.elastic.address.search;

import java.util.List;

import smart.elastic.address.text.NGram;

public enum SearchType {

    INDEXED("name"), FULL_INDEXED("full_name"), ORIGINAL("original_name"), FULL_ORIGINAL("original_full_name");

    private final String field;

    private SearchType(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public List<String> getTokens(NGram nGram) {
        if (INDEXED.equals(this) || FULL_INDEXED.equals(this))
            return nGram.getAnalyzedTokens();
        if (ORIGINAL.equals(this) || FULL_ORIGINAL.equals(this))
            return nGram.getTokens();
        return null;
    }

}
