package smart.elastic.address.text;

import java.util.List;

import org.elasticsearch.common.base.Joiner;

import smart.elastic.address.Type;
import smart.elastic.address.config.Mappings;

public class NGram {

    private transient int dimension;

    private final transient List<String> tokens;

    private final String token;

    private final transient List<String> analyzedTokens;

    private final String analyzedToken;

    private final transient List<String> cleanedTokens;

    private final transient Type type;

    private final transient int startIndex;

    private final transient int endIndex;

    public NGram(Type type, int dimension, List<String> tokens, List<String> analyzedTokens, int startIndex) {
        this.type = type;
        this.startIndex = startIndex;
        this.dimension = dimension;
        this.endIndex = startIndex + dimension;
        this.tokens = tokens;
        this.analyzedTokens = analyzedTokens;
        this.token = Joiner.on(' ').join(tokens);
        this.analyzedToken = Joiner.on(' ').join(analyzedTokens);
        this.cleanedTokens = TextProcessor.getInstance().excludeTokens(tokens, Mappings.getObjectTypes(type));
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public String getToken() {
        return token;
    }

    public List<String> getAnalyzedTokens() {
        return analyzedTokens;
    }

    public String getAnalyzedToken() {
        return analyzedToken;
    }

    public List<String> getCleanedTokens() {
        return cleanedTokens;
    }

    public Type getType() {
        return type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public boolean isLongEnough(int len) {
        for (String token : analyzedTokens)
            if (token.length() >= len)
                return true;
        return false;
    }

    public int getEndIndex() {
        return endIndex;
    }
}
