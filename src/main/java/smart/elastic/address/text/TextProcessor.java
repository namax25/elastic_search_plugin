package smart.elastic.address.text;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import smart.elastic.address.Type;
import smart.elastic.address.config.Mappings;
import smart.elastic.address.search.SearchExecutor;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

public class TextProcessor {

    public static final Pattern cleanPattern = Pattern.compile("[^а-яё0-9]");

    public static final Pattern nbspPattern = Pattern.compile("\\s{2,}");

    public static final Pattern digitsPattern = Pattern.compile("^\\d+$");

    public static final String NBSP = " ";

    private static final List<Pattern> CUSTOM_ENDING_PATTERNS = new ArrayList<Pattern>();

    @SuppressWarnings("serial")
    private static final List<Pattern> housePatterns = new ArrayList<Pattern>() {{
        final int flags = Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE;
        add(Pattern.compile("^(\\d{1,3})$", flags));
        add(Pattern.compile("^(\\d{1,3})(к\\d{1,2})$", flags));
        add(Pattern.compile("^(\\d{1,3})(к[а-я0-9]{1,3})$", flags));
        add(Pattern.compile("^(\\d{1,3})(к\\d{1,2}с\\d{1,2})$"));
        add(Pattern.compile("^(\\d{1,3})(с\\d{1,2})$"));
        add(Pattern.compile("^(\\d{1,3})([а-я])$"));
    }};

    private static final Map<String, String> houseReplacePatterns = new LinkedHashMap<String, String>() {
        {
            put("корпус", "к");
            put("кор", "к");
            put("кк", "к");
            put("корпуса", "к");
            put("строение", "с");
            put("стр", "с");
            put("владение", "вл");
        }

    };

    static {
        for (String ending : Arrays.asList("ая", "ый", "ий", "ой", "я", "й")) {
            CUSTOM_ENDING_PATTERNS.add(Pattern.compile("\\s" + ending + "\\s"));
        }
    }

    private static final TextProcessor instance = new TextProcessor();

    public static TextProcessor getInstance() {
        return instance;
    }

    public TextResponse process(SearchExecutor searchExecutor, String originalText, Type type) {
        if (originalText == null || originalText.trim().length() == 0)
            return null;
        String cleanedText = cleanText(originalText);
        List<String> tokens = new ArrayList<String>();
        for (String token : Splitter.on(NBSP).split(cleanedText))
            tokens.add(Type.METRO.equals(type) ? Mappings.getMetroMapping(token) : Mappings.getMapping(token));
        cleanedText = Joiner.on(NBSP).join(tokens);
        List<String> analyzedTokens = searchExecutor.analyzeText(cleanedText);
        String analyzedText = Joiner.on(NBSP).join(analyzedTokens);
        return new TextResponse(originalText, cleanedText, analyzedText, tokens, analyzedTokens);
    }

    public List<NGram> getNGrams(int size, TextResponse textResponse, Type type) {
        if (textResponse.getTokens().size() < size)
            return Collections.emptyList();

        int count = Math.min(textResponse.getTokens().size(), textResponse.getAnalyzedTokens().size()) - size + 1;
        Map<String, Boolean> cache = new HashMap<String, Boolean>();
        List<NGram> result = new ArrayList<NGram>();
        for (int i = 0; i < count; i++) {
            NGram nGram = new NGram(type, size, textResponse.getTokens().subList(i, i + size), textResponse.getAnalyzedTokens().subList(i, i + size), i);
            if (cache.containsKey(nGram.getToken()))
                continue;
            cache.put(nGram.getToken(), true);
            result.add(nGram);
        }
        return result;
    }

    public List<NGram> filterNGrams(List<NGram> nGrams, int minSize) {
        List<NGram> result = new ArrayList<NGram>();
        for (NGram nGram : nGrams) {
            if (Mappings.getObjectTypes(nGram.getType()).contains(nGram.getToken()))
                continue;
            if (!nGram.isLongEnough(minSize))
                continue;
            if (digitsPattern.matcher(nGram.getToken()).matches() || digitsPattern.matcher(nGram.getAnalyzedToken()).matches())
                continue;
            result.add(nGram);
        }
        return result;
    }

    public String cleanText(String text) {
        String cleanedText = text.toLowerCase();
        cleanedText = NBSP + cleanPattern.matcher(cleanedText).replaceAll(NBSP) + NBSP;
        cleanedText = cleanedText.replace("ё", "е");
        for (Pattern p : CUSTOM_ENDING_PATTERNS)
            cleanedText = p.matcher(cleanedText).replaceAll(NBSP);
        cleanedText = nbspPattern.matcher(cleanedText).replaceAll(NBSP);
        return cleanedText.trim();
    }

    public List<String> excludeTokens(List<String> tokens, List<String> excludes) {
        List<String> cleaned = new ArrayList<String>();
        for (String token : tokens) {
            if (!excludes.contains(token))
                cleaned.add(token);
        }
        return cleaned;
    }

    public List<String> getHouse(String token) {

        String tokenReplaced = token;
        for (Map.Entry<String, String> entry : houseReplacePatterns.entrySet()) {

            if (token.contains(entry.getKey())) {
                tokenReplaced = token.replaceAll(entry.getKey(), entry.getValue());
                break;
            }

        }

        for (Pattern p : housePatterns) {
            Matcher matcher = p.matcher(tokenReplaced);
            if (matcher.matches()) {
                List<String> result = new ArrayList<String>(2);
                for (int i = 1; i <= matcher.groupCount(); i++)
                    result.add(matcher.group(i));
                if (matcher.groupCount() < 2)
                    result.add(null);
                return result;
            }
        }
        return null;
    }
}
