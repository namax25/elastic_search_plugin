package smart.elastic.address.text;

import java.util.ArrayList;
import java.util.List;


public class TextResponse {
    private final String originalText;

    private final String cleanedText;

    private final String analyzedText;

    private final List<String> tokens;

    private final List<String> analyzedTokens;

    public TextResponse(String originalText, String cleanedText, String analyzedText, List<String> tokens, List<String> analyzedTokens) {
        this.originalText = originalText;
        this.cleanedText = cleanedText;
        this.analyzedText = analyzedText;
        this.tokens = tokens;
        this.analyzedTokens = analyzedTokens;
    }

    public String getOriginalText() {
        return originalText;
    }

    public String getCleanedText() {
        return cleanedText;
    }

    public String getAnalyzedText() {
        return analyzedText;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public List<String> getAnalyzedTokens() {
        return analyzedTokens;
    }

    public TextResponse cutNGram(NGram nGram) {
        List<String> newTokens = new ArrayList<String>(tokens);
        newTokens.removeAll(tokens.subList(nGram.getStartIndex(), nGram.getStartIndex() + nGram.getDimension()));

        List<String> newAnalyzedTokens = new ArrayList<String>(analyzedTokens);
        newAnalyzedTokens.removeAll(analyzedTokens.subList(nGram.getStartIndex(), nGram.getStartIndex() + nGram.getDimension()));

        return new TextResponse(originalText, cleanedText, analyzedText, newTokens, newAnalyzedTokens);
    }
}
