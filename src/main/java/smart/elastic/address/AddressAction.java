package smart.elastic.address;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.RestRequest.Method;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.rest.StringRestResponse;
import org.elasticsearch.rest.XContentThrowableRestResponse;

import smart.elastic.address.config.Config;
import smart.elastic.address.config.Mappings;
import smart.elastic.address.response.Item;
import smart.elastic.address.response.Result;
import smart.elastic.address.response.SearchResponse;
import smart.elastic.address.search.SearchExecutor;
import smart.elastic.address.search.SearchType;
import smart.elastic.address.text.NGram;
import smart.elastic.address.text.TextProcessor;
import smart.elastic.address.text.TextResponse;
import smart.elastic.address.utils.Pair;
import smart.elastic.address.utils.Triple;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

public class AddressAction extends BaseRestHandler {

    private ESLogger log = Loggers.getLogger(getClass());

    private SearchExecutor searchExecutor = new SearchExecutor(client, Config.get("index").toString());

    private int minTokenLength = Integer.valueOf(Config.get("minTokenLength").toString());

    private int countyMaxTokenSize = Integer.valueOf(Config.get("countyMaxTokenSize").toString());

    private int streetMaxTokenSize = Integer.valueOf(Config.get("streetMaxTokenSize").toString());

    private int fullAddressMaxTokenSize = Integer.valueOf(Config.get("fullAddressMaxTokenSize").toString());

    @SuppressWarnings("serial")
    private final Map<Integer, Float> fuzziness = new HashMap<Integer, Float>() {{
        put(5, 0.7f);
        put(4, 0.7f);
        put(3, 0.7f);
        put(2, 0.7f);
        put(1, 1f);
    }};

    @Inject
    protected AddressAction(Settings settings, Client client, RestController controller) {
        super(settings, client);
        controller.registerHandler(Method.GET, "/_address", this);
    }

    @Override
    public void handleRequest(final RestRequest request, final RestChannel channel) {

        try {
            TextResponse text = TextProcessor.getInstance().process(searchExecutor, request.param("text"), Type.STREET);
            if (text == null) {
                channel.sendResponse(new StringRestResponse(RestStatus.OK, Result.NULL.toJSON()));
                return;
            }
            List<Integer> parents = null;
            if (request.param("parents") != null) {
                parents = new ArrayList<Integer>();
                for (String p : Splitter.on(',').omitEmptyStrings().trimResults().split(request.param("parents")))
                    parents.add(Integer.valueOf(p));
            }
            if (parents != null && parents.size() == 1 && -1 == parents.get(0)) {
                parents = Collections.emptyList();
            } else if (parents == null || parents.size() == 0) {
                parents = Arrays.asList(46, 47);
            }

            SearchResponse response = performSearch(text, parents);

            if (response.getStreet() == null || response.getCounty() == null) {
                SearchResponse fullAddress = getStreetByFullAddress(text, parents);
                if (!SearchResponse.NULL.equals(fullAddress))
                    response = fullAddress;
            }

            if (response.getStreet() != null && response.getCounty() == null) {
                Item county = getCountyByParent(response.getStreets());
                response = new SearchResponse(county, response.getStreet(), response.getStreets());
            }

            String cacheName = buildCacheName(response);

            Pair<String, String> house = null;
            if (response.getStreet() != null)
                house = getHouse(response.getStreet());

            Result result = new Result(text, response.getCounty(), response.getStreet(), response.getStreets(), cacheName, house == null ? null : house.getFirst(), house == null ? null : house.getSecond());


            channel.sendResponse(new StringRestResponse(RestStatus.OK, result.toJSON()));
        } catch (Exception e) {
            try {
                e.printStackTrace();
                channel.sendResponse(new XContentThrowableRestResponse(request, e));
            } catch (IOException e1) {
                e1.printStackTrace();
                log.error("something gone wrong", e1);
            }
        }
    }

    private List<Integer> getCommonParents(List<Item> streets) {
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < streets.get(0).getParents().size(); i++) {
            Integer id = streets.get(0).getParents().get(i);
            boolean matched = true;
            for (Item street : streets) {
                if (street.getParents().size() <= i) {
                    matched = false;
                    break;
                }
                if (street.getParents().get(i).intValue() != id.intValue()) {
                    matched = false;
                    break;
                }
            }
            if (matched)
                result.add(id);
            else
                break;
        }
        return result;
    }

    private String buildCacheName(SearchResponse response) throws InterruptedException, ExecutionException {
        if (SearchResponse.NULL.equals(response))
            return null;
        List<Item> counties = null;
        if (response.getStreets() != null && response.getStreets().size() > 0) {
            List<Integer> commonParents = getCommonParents(response.getStreets());
            if (commonParents.size() > 0) {
                counties = searchExecutor.getParents(Type.COUNTY, commonParents);
            }
        } else {
            if (response.getCounty() != null)
                counties = searchExecutor.getParents(Type.COUNTY, response.getCounty().getParents());
        }
        List<String> result = new ArrayList<String>();
        if (counties != null && counties.size() > 0) {
            for (Item county : counties)
                result.add(county.getShortName() + ' ' + county.getOriginalName());
        }
        if (response.getStreet() != null)
            result.add(response.getStreet().getShortName() + ' ' + response.getStreet().getOriginalName());

        for (int i = 0; i < result.size(); i++) {
            String token = result.get(i);
            for (String mapping : Mappings.getOutputMappings().keySet()) {
                if (token.startsWith(mapping)) {
                    result.set(i, token.replace(mapping, Mappings.getOutputMappings().get(mapping)));
                    break;
                }
            }
        }
        return result.size() > 0 ? Joiner.on(", ").join(result) : null;
    }

    private SearchResponse performSearch(TextResponse text, List<Integer> parents) throws Exception {
        List<Item> counties = getCounties(text, parents);
        if (counties.size() == 0) {
            Pair<Item, List<Item>> streetResponse = getStreet(text, parents);
            if (streetResponse == null)
                return SearchResponse.NULL;
            return new SearchResponse(null, streetResponse.getFirst(), filterStreetsResult(streetResponse.getFirst(), streetResponse.getSecond()));
        }

        for (Item county : counties) {
            TextResponse countyResponse = text.cutNGram(county.getnGram());
            Pair<Item, List<Item>> streetResponse = getStreet(countyResponse, Arrays.asList(county.getId()));
            if (streetResponse != null)
                return new SearchResponse(county, streetResponse.getFirst(), filterStreetsResult(streetResponse.getFirst(), streetResponse.getSecond()));
        }

        return new SearchResponse(counties.get(0), null, null);
    }

    private Pair<String, String> getHouse(Item street) {
        TextResponse text = street.getText();
        if (text == null)
            return null;
        List<String> houseTokens = new ArrayList<String>();
        if (text.getTokens().size() > street.getnGram().getEndIndex())
            houseTokens.add(text.getTokens().get(street.getnGram().getEndIndex()));
        if (text.getTokens().size() > street.getnGram().getEndIndex() + 1)
            houseTokens.add(text.getTokens().get(street.getnGram().getEndIndex() + 1));
        if (text.getTokens().size() > street.getnGram().getEndIndex() + 2)
            houseTokens.add(text.getTokens().get(street.getnGram().getEndIndex() + 2));

        if (houseTokens.isEmpty())
            return null;
        Pair<String, String> result = new Pair<String, String>();

        List<String> house1 = TextProcessor.getInstance().getHouse(houseTokens.get(0));
        List<String> house2 = houseTokens.size() > 1 ? TextProcessor.getInstance().getHouse(houseTokens.get(1)) : null;
        if (house1 != null) {
            result.setFirst(house1.get(0));
            result.setSecond(house1.get(1));
        }
        if (result.getFirst() != null && result.getSecond() != null)
            return result;

        setHouseResult(result, house2);

        if (result.getFirst() != null && result.getSecond() != null)
            return result;

        if (houseTokens.size() > 1) {
            StringBuilder allTokens = new StringBuilder();
            for (String token : houseTokens) {
                allTokens.append(token);
            }

            List<String> house3 = TextProcessor.getInstance().getHouse(allTokens.toString());

            if (house3 != null) {
                if (result.getFirst() == null) {
                    result.setFirst(house3.get(0));
                    result.setSecond(house3.get(1));
                } else {
                    result.setSecond(house3.get(1));
                }
            }
        }

        return result;
    }

    private void setHouseResult(Pair<String, String> result, List<String> house) {
        if (house != null) {
            if (result.getFirst() == null) {
                result.setFirst(house.get(0));
                result.setSecond(house.get(1));
            } else {
                result.setSecond(Joiner.on(' ').join(house));
            }
        }
    }

    private Item getCountyByParent(List<Item> streets) throws InterruptedException, ExecutionException {
        Item street = streets.get(0);
        //проверим, что все улицы в одном городе
        for (Item s : streets) {
            if (!street.getRealParents().equals(s.getRealParents()))
                return null;
        }
        List<Item> counties = searchExecutor.getParents(Type.COUNTY, street.getParents());
        Collections.sort(counties, new ParentsComparator());
        Collections.reverse(counties);
        for (Item county : counties) {
            if (Mappings.getCountyTypesMap().containsKey(county.getShortName()))
                return county;
        }
        if (counties.size() > 0)
            return counties.get(0);
        return null;
    }

    //оставим улицы только с одинаковым именем
    private List<Item> filterStreetsResult(Item street, List<Item> streets) {
        List<Item> result = new ArrayList<Item>();
        for (Item s : streets) {
            //|| !s.getShortName().equals(street.getShortName())
            if (!s.getToken().equals(street.getToken()))
                continue;
            result.add(s);
        }
        return result;
    }

    private List<Item> getCounties(TextResponse text, List<Integer> parents) throws Exception {
        List<Item> counties = new ArrayList<Item>();
        for (int i = countyMaxTokenSize; i > 0; i--) {
            List<NGram> nGrams = TextProcessor.getInstance().filterNGrams(TextProcessor.getInstance().getNGrams(i, text, Type.COUNTY), minTokenLength);
            for (SearchType searchType : Arrays.asList(SearchType.FULL_ORIGINAL, SearchType.ORIGINAL)) {
                for (NGram nGram : nGrams) {
                    List<Item> items = searchExecutor.search(nGram, Type.COUNTY, parents, searchType, text);
                    if (items.size() == 0)
                        continue;
                    Item county = analyzeCountyResponse(items);
                    if (county != null)
                        counties.add(county);
                }
            }
        }
        if (counties.isEmpty()) {
            for (int i = fullAddressMaxTokenSize; i >= 3; i--) {
                List<NGram> nGrams = TextProcessor.getInstance().filterNGrams(TextProcessor.getInstance().getNGrams(i, text, Type.COUNTY), minTokenLength);
                for (NGram nGram : nGrams) {
                    List<Item> items = searchExecutor.searchInFullAddress(Type.COUNTY, nGram, parents, text);
                    if (items.size() != 1) {
                        continue;
                    }
                    counties.add(items.get(0));
                }
            }
        }
        Collections.sort(counties, new DepthComparator());
        return counties;
    }

    private SearchResponse getStreetByFullAddress(TextResponse text, List<Integer> parents) throws Exception {
        for (int i = fullAddressMaxTokenSize; i >= 3; i--) {
            List<NGram> nGrams = TextProcessor.getInstance().filterNGrams(TextProcessor.getInstance().getNGrams(i, text, Type.STREET), minTokenLength);
            for (NGram nGram : nGrams) {
                List<Item> items = searchExecutor.searchInFullAddress(Type.STREET, nGram, parents, text);
                if (items.size() != 1)
                    continue;
                return new SearchResponse(null, items.get(0), Arrays.asList(items.get(0)));
            }
        }
        return SearchResponse.NULL;
    }

    private Pair<Item, List<Item>> getStreet(TextResponse text, List<Integer> parents) throws Exception {
        for (int i = streetMaxTokenSize; i > 0; i--) {
            List<NGram> nGrams = TextProcessor.getInstance().filterNGrams(TextProcessor.getInstance().getNGrams(i, text, Type.STREET), minTokenLength);
            for (SearchType searchType : Arrays.asList(SearchType.FULL_ORIGINAL, SearchType.ORIGINAL, SearchType.FULL_INDEXED, SearchType.INDEXED)) {
                for (NGram nGram : nGrams) {
                    if (nGram.getTokens().size() < 3 && (SearchType.FULL_INDEXED.equals(searchType) || SearchType.INDEXED.equals(searchType)))
                        continue;
                    List<Item> items = searchExecutor.search(nGram, Type.STREET, parents, searchType, text);
                    if (items.size() == 0)
                        continue;
                    Item street = analyzeStreetResponse(items);
                    if (street != null)
                        return new Pair<Item, List<Item>>(street, items);
                }
            }
        }
        return null;
    }

    private Triple<String, Map<String, List<Item>>, Integer> getMatchesForAnalyze(List<Item> items) {
        String maxToken = null;
        int total = 0;
        Map<String, List<Item>> matches = new HashMap<String, List<Item>>();
        for (Item item : items) {
            boolean fullSearch = SearchType.FULL_INDEXED.equals(item.getSearchType()) || SearchType.FULL_ORIGINAL.equals(item.getSearchType());
            List<String> nGramTokens = fullSearch ? item.getnGram().getTokens() : item.getnGram().getCleanedTokens();
            List<String> itemTokens = fullSearch ? item.getFullOriginalTokens() : item.getOriginalTokens();
            if (itemTokens.size() != nGramTokens.size())
                continue;
            total++;
            String token = item.getToken();
            if (!matches.containsKey(token))
                matches.put(token, new ArrayList<Item>());
            matches.get(token).add(item);
            if (maxToken == null || matches.get(token).size() >= matches.get(maxToken).size())
                maxToken = token;
        }
        return new Triple<String, Map<String, List<Item>>, Integer>(maxToken, matches, total);
    }

    private Item analyzeStreetResponse(List<Item> streets) {
        Triple<String, Map<String, List<Item>>, Integer> matches = getMatchesForAnalyze(streets);
        if (matches.getFirst() == null)
            return null;
        List<Item> matched = matches.getSecond().get(matches.getFirst());
        Item street = matched.get(0);
        //если искали по одному слову, то результаты должны совпадать по имени
        if (street.getnGram().getTokens().size() == 1 && matches.getSecond().keySet().size() > 1)
            return null;

        //если искали по одному слову, то результаты должны быть одного типа
        if (street.getnGram().getTokens().size() == 1) {
            for (Item item : matched) {
                if (!item.getShortName().equals(street.getShortName()))
                    return null;
            }
        }

        if (matched.size() / matches.getThird() < fuzziness.get(street.getnGram().getDimension()))
            return null;

        return street;
    }

    private Item analyzeCountyResponse(List<Item> counties) {
        Triple<String, Map<String, List<Item>>, Integer> matches = getMatchesForAnalyze(counties);
        if (matches.getFirst() == null)
            return null;
        List<Item> matched = matches.getSecond().get(matches.getFirst());
        Item county = matched.get(0);

        if (matched.size() / matches.getThird() < fuzziness.get(county.getnGram().getDimension()))
            return null;
        return county;
    }

    private class ParentsComparator implements Comparator<Item> {
        @Override
        public int compare(Item item1, Item item2) {
            if (item1.getId() == item2.getId())
                return 0;
            return item1.getId() < item2.getId() ? -1 : 1;
        }
    }

    private class DepthComparator implements Comparator<Item> {
        @Override
        public int compare(Item item1, Item item2) {
            if (item1.getParents().size() == item2.getParents().size())
                return 0;
            return item1.getParents().size() < item2.getParents().size() ? 1 : -1;
        }
    }
}
